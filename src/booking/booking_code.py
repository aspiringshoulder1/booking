def basic_information():
    customer_name = input("Please enter the customer's name: ")
    package_info = input("Please enter information about the package: ")
    danger = input("Are the contents of the package dangerous? Enter 1 for yes and 2 for no:")
    weight = input("Please enter the weight of the package in kilograms: ")
    volume = input("Please enter the volume of the package in square meters: ")
    required_date = input("Please enter the date that the package must be delivered by in mm/dd/yyyy format: ")
    today = date.today()
    business_day_gap = "business days between today and required delivery date"  # is placeholder

    ship_it = False
    if weight < 10 or volume < 5:
        ship_it = True

    fly_it = True
    if danger == 1:
        fly_it = False

    urgent = False
    if business_day_gap <= 3:
        urgent = True

    fly_weight_cost = 10 * weight
    fly_volume_cost = 20 * volume
    if fly_weight_cost >= fly_volume_cost:
        fly_final_cost = fly_weight_cost
    else:
        fly_final_cost = fly_volume_cost

    truck_shipment_cost = 25
    if urgent == True:
        truck_shipment_cost = 45

    ocean_shipment_cost = 30

    return








